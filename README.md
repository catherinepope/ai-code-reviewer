## Code Reviewer

This Python script takes a filename as an argument, then performs a code review on that file. It makes suggestions and offers an improved version of the code.

To use it, you'll need to add your OpenAI API key in an `.env` file.

Based on a project in Colt Steele's [Mastering OpenAI Python APIs course](https://www.udemy.com/course/mastering-openai/).
